# FreePBX on Docker

### Image includes

 * phusion/baseimage (Ubuntu 14.04)
 * LAMP stack (apache2, mysql, php)
 * Asterisk 13
 * FreePBX 13
 

### Build your FreePBX Image
```bash
docker build -t "adm1nx/freepbx" .
```
### Run your FreePBX image
```bash
docker run --net=host -d -t adm1nx/freepbx
```

Test it out by visiting your hosts ip address in a browser.

### Fork ME

Forked from jmar71n/docker-freepbx
[https://bitbucket.org/jmar71n/docker-freepbx/src/]